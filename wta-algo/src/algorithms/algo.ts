import quicksortFromTs from "./tsQuicksort";
import quicksortFromCs from "./csQuicksort";
import init, { Algo } from "../../pkg/algo";

let sampleSize = 0;
let onlySample = false;

onmessage = async (e) => {
  console.log("message received in myWorker");
  console.log(e.data);
  // validate input
  if (e.data === null) {
    console.log("data was null");
    postMessage({ status: false });
  }

  // do something
  sampleSize = await e.data.sampleSize;
  onlySample = e.data.onlySample;

  // run tests
  await testAlgorithm(sortTs);
  await testWasm(sampleSize);
  await testAlgorithm(sortCs);
  postMessage({ status: "done" });
};

const sortTs = (arrToSort) =>
  quicksortFromTs(arrToSort, 0, arrToSort.length - 1);
const sortCs = (arrToSort) =>
  quicksortFromCs(arrToSort, 0, arrToSort.length - 1);

// Algo Helpers
export const getRandomArray = async (
  sampleSize: number
): Promise<Array<number>> => {
  let arr = [];
  for (let i = 0; i < sampleSize; i++) {
    arr.push(Math.floor(Math.random() * (sampleSize + 1)));
  }
  return arr;
};
const logSample = (sampleArray?: Array<number>, size?: number): void => {
  console.log(sampleArray?.slice(0, size));
};

// PostMessage Helpers
const postMessageToggleAlgoStatus = (algorithmName) => {
  postMessage({
    status: "alive",
    algo: algorithmName,
    type: "toggle",
  });
};
const postMessageSampleStart = (algorithmName: String, start: number) => {
  postMessage({
    status: "alive",
    algo: algorithmName,
    type: "sampleStart",
    times: { start },
  });
};
const postMessageSampleEnd = (algorithmName: String, end: number) => {
  postMessage({
    status: "alive",
    algo: algorithmName,
    type: "sampleEnd",
    times: { end },
  });
};
const postMessageAlgoStart = (algorithmName: String, start: number) => {
  postMessage({
    status: "alive",
    algo: algorithmName,
    type: "algoStart",
    times: { start },
  });
};
const postMessageAlgoEnd = (algorithmName: String, end: number) => {
  postMessage({
    status: "alive",
    algo: algorithmName,
    type: "algoEnd",
    times: { end },
  });
};

const testAlgorithm = async (
  algorithm: (args: Array<number>) => void
): Promise<void> => {
  // toggle status
  await postMessageToggleAlgoStatus(algorithm.name);

  // generate sample
  let start = await performance.now();
  await postMessageSampleStart(algorithm.name, start);
  let sampleArray = await getRandomArray(sampleSize);
  let end = await performance.now();
  await postMessageSampleEnd(algorithm.name, end);

  // sort sample
  if (!onlySample) {
    start = await performance.now();
    await postMessageAlgoStart(algorithm.name, start);
    await algorithm(sampleArray);
    end = await performance.now();
    await postMessageAlgoEnd(algorithm.name, end);
  }

  // toggle status
  await postMessageToggleAlgoStatus(algorithm.name);

  // log sample
  logSample(sampleArray, 10);
};

const testWasm = (sampleSize: number) => {
  init()
    .then((_) => {
      postMessageToggleAlgoStatus("sortWasm");

      // generate sample
      let start = performance.now();
      postMessageSampleStart("sortWasm", start);
      const algo = Algo.new(sampleSize);
      let end = performance.now();
      postMessageSampleEnd("sortWasm", end);

      console.log(algo.size());
      // logSample(Array.from(algo.sample_vec()), 10);

      return algo;
    })
    .then((algo) => {
      if (!onlySample) {
        let start = performance.now();
        postMessageAlgoStart("sortWasm", start);
        console.log("passes here");
        try {
          algo.sort();
        } catch (e) {
          console.error(e);
        }
        console.log("and here");
        let end = performance.now();
        postMessageAlgoEnd("sortWasm", end);
        try {
          logSample(Array.from(algo.sample_vec()), 10);
        } catch (e) {
          console.error(e);
        }
      }
      // toggle status
      postMessageToggleAlgoStatus("sortWasm");
    });
};
