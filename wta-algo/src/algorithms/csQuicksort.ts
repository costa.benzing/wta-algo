const swap = (a: Array<number>, iFirst: number, iSecond: number) => {
  const temp = a[iFirst];
  a[iFirst] = a[iSecond];
  a[iSecond] = temp;
};

const partition = (a: Array<number>, iStart: number, iEnd: number): number => {
  const pivot = a[iEnd];
  let iPivot = iStart;
  for (let i = iStart; i < iEnd; i++) {
    if (a[i] <= pivot) {
      swap(a, i, iPivot);
      iPivot++;
    }
  }
  swap(a, iEnd, iPivot);
  return iPivot;
};

const csQuicksort = (a: Array<number>, iStart: number, iEnd: number) => {
  if (iStart >= iEnd) return;
  const iPivot = partition(a, iStart, iEnd);
  csQuicksort(a, iStart, iPivot - 1);
  csQuicksort(a, iPivot + 1, iEnd);
};

export default csQuicksort;
