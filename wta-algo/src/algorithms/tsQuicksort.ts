export const swap = (a: Array<number>, iFirst: number, iSecond: number) => {
  const temp = a[iFirst];
  a[iFirst] = a[iSecond];
  a[iSecond] = temp;
};

export const partition = (
  a: Array<number>,
  iStart: number,
  iEnd: number
): number => {
  const pivot = a[iEnd];
  let iPivot = iStart;
  for (let i = iStart; i < iEnd; i++) {
    if (a[i] <= pivot) {
      swap(a, i, iPivot);
      iPivot++;
    }
  }
  swap(a, iEnd, iPivot);
  return iPivot;
};

export const tsQuicksort = (a: Array<number>, iStart: number, iEnd: number) => {
  if (iStart >= iEnd) return;
  const iPivot = partition(a, iStart, iEnd);
  tsQuicksort(a, iStart, iPivot - 1);
  tsQuicksort(a, iPivot + 1, iEnd);
};

export const sort = (a: Array<number>) => {
  tsQuicksort(a, 0, a.length - 1);
};

export default tsQuicksort;
