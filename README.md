# SVELTE

## Getting started

```
cd wta-algo
npm i
```

## Test and Development

Run the Vite development server
```
cd wta-algo
npm run dev
```

## Build and Deploy

First:
```
cd wta-algo
npm run build
```

Then:

```
cd ../algo
/bin/zsh ./copy.sh
```

The second step builds the current Rust-wasm project and copies all necessary files into the Svelte development environment and into the dist folder under assets.

Place all dist files in the root of your hosted domain.