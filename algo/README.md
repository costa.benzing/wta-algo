# ALGO RUST

## Description
Algo is a cargo project. Here a lib.rs file is provided with rust code. The lib provides a class Algo, its implementation and a quicksort alogrithm, which can be exported to a wasm-pack using the build command provided below. 

## Getting started
Just follow the build instructions to create a pkg folder. This is the glue kit between the Svelte project and the created wasm module.

## Installation


### Build the code
```
cd algo
wasm-pack build --target web
```

### Inject the built package into the Svelte project

In order to automatically paste the compiled code into the Svelte project execute the provided script copy.sh. Using a z-shell terminal the command would look like this

```
cd algo
/bin/zsh ./copy.sh
```