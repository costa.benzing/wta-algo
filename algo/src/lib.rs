use wasm_bindgen::prelude::*;
use wee_alloc::WeeAlloc;
use rand;

#[global_allocator]
static ALLOC: WeeAlloc = WeeAlloc::INIT;

#[wasm_bindgen]
pub struct Algo {
    size: i32,
    sample_vec: Vec<i32>
}

#[wasm_bindgen]
impl Algo {
    pub fn new(size: i32) -> Algo {
        Algo {
            size,
            sample_vec: generate_sample(size)
        }
    }

    pub fn size(&self) -> i32 {
        self.size
    }
    pub fn sample_vec(self) -> Vec<i32> {
        self.sample_vec
    }
    pub fn set_size(&mut self, size: i32) {
        self.size = size
    }

    pub fn sort(&mut self) {
        let i: usize = self.size as usize;
        quicksort(&mut self.sample_vec, 0, i);
    }
}

fn generate_sample(size: i32) -> Vec<i32> {
    let mut vec: Vec<i32> = Vec::new();
    for _ in 0..size {
        vec.push(rand::random());
    }
    vec
}

fn quicksort(a: &mut Vec<i32>, start: usize, end: usize) {
    if start >= end { return; }
    let i_pivot = partition(a, start, end);
    quicksort(a, start, i_pivot - 1);
    quicksort(a, i_pivot + 1, end);
}

fn partition(a: &mut Vec<i32>, start: usize, end: usize) -> usize {
    let value = a[end];
    let mut pivot = start;
    for i in start..end {
        if a[i] <= value {
            a.swap(i, pivot);
            pivot += 1;
        }
    }
    a.swap(end as usize, pivot as usize);
    pivot
}




// #[wasm_bindgen]
// extern {
//     pub fn alert(s: &str);
// }
//
// #[wasm_bindgen]
// pub fn greet(name: &str) {
//     alert(&format!("Hello, {}!", name));
// }


// import external files to use their functions in .wasm
// path is defined as absolute path from project root
// #[wasm_bindgen(module = "/src/sample.ts")]
// extern {
//     fn getSampleSize() -> i32;
// }
//
// #[wasm_bindgen]
// pub fn show() -> i32 {
//     getSampleSize()
// }